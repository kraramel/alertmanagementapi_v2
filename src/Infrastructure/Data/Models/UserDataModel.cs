﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class UserDataModel 
    {
        public int Id { get; set; }
        public DateTime RegistrationDate { get; set; }
        public bool IsActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsArchived { get; set; }

        public string Profil { get; set; }
        public int DomainId { get; set; }
        [JsonIgnore]
        public DomainDataModel Domain { get; set; }
        
/*        public IList<UserGroup> UserGroup { get; set; }
*/        public bool IsAdminDomain { get; set; }
        public string ImgPath { get; set; }
        public string Name { get; set; }
        public DateTime ExpirationPassword { get; set; }

        public List<AlertNotificationUserDataModel> AlertNotificationUsers { get; set; }
        public List<ScalingUserDataModel> ScalingUsers { get; set; }
    }
   
}
