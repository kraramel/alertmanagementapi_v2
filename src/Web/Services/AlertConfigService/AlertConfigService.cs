﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using Web.Controllers.AlertConfig.Model;
using Web.Exceptions;

namespace Web.Services.AlertConfigService
{
    public class AlertConfigService : IAlertConfigService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public AlertConfigService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<List<AlertConfigResponse>> AddAlertConfig(CreateAlertConfigQuery newAlertConfig)
        {
            List<AlertConfigResponse> serviceResponse = new List<AlertConfigResponse>();

            _context.Add(_mapper.Map<AlertConfigDataModel>(newAlertConfig));
            await _context.SaveChangesAsync();
            List<AlertConfigDataModel> dbAlertConfigs = await _context.AlertConfigs.Include(i => i.AlertNotifications)
                                                                          .Include(i => i.Scalings).ToListAsync();

            serviceResponse = dbAlertConfigs.Select(c => _mapper.Map<AlertConfigResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<List<AlertConfigResponse>> GetAllAlertConfigs()
        {
            List<AlertConfigResponse> serviceResponse = new List<AlertConfigResponse>();

            //Get the Characters from the database
            List<AlertConfigDataModel> dbAlertConfigs = await _context.AlertConfigs.Include(i => i.AlertNotifications)
                                                                          .Include(i => i.Scalings)
                                                                          .ToListAsync();

            serviceResponse= dbAlertConfigs.Select(c => _mapper.Map<AlertConfigResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<AlertConfigResponse> GetAlertConfigById(int id)
        {
            AlertConfigResponse serviceResponse = new AlertConfigResponse();
            AlertConfigDataModel dbAlertConfig = await _context.AlertConfigs.Include(i => i.AlertNotifications)
                                                                   .Include(i => i.Scalings)
                                                                   .FirstOrDefaultAsync(c => c.Id == id);
            serviceResponse = _mapper.Map<AlertConfigResponse>(dbAlertConfig);
            return serviceResponse;
        }


        public async Task<AlertConfigResponse> UpdateAlertConfig(UpdateAlertConfigQuery updatedAlertConfig, int id)
        {
            AlertConfigResponse serviceResponse = new AlertConfigResponse();
            try
            {
                AlertConfigDataModel alertConfig = await _context.AlertConfigs.Include(i => i.AlertNotifications)
                                                                     .Include(i => i.Scalings)
                                                                     .FirstOrDefaultAsync(c => c.Id == id);
                if (alertConfig == null)
                {
                    throw new CommandeNotFoundException();
                }

                alertConfig.Designation = updatedAlertConfig.Designation;
                alertConfig.NiveauCriticite = updatedAlertConfig.NiveauCriticite;
                alertConfig.Etat = updatedAlertConfig.Etat;
                alertConfig.TextAlert = updatedAlertConfig.TextAlert;
                alertConfig.Module = updatedAlertConfig.Module;
                alertConfig.Frequence = updatedAlertConfig.Frequence;
                alertConfig.DateDebut = updatedAlertConfig.DateDebut;
                alertConfig.DateFin = updatedAlertConfig.DateFin;
                alertConfig.SourceDonnee = updatedAlertConfig.SourceDonnee;

                _context.AlertConfigs.Update(alertConfig);
                await _context.SaveChangesAsync();

                serviceResponse = _mapper.Map<AlertConfigResponse>(alertConfig);
            }
            catch (Exception ex)
            {
                
            }
            return serviceResponse;
        }

        public async Task<List<AlertConfigResponse>> DeleteAlertConfig(int id)
        {
            List<AlertConfigResponse> serviceResponse = new List<AlertConfigResponse>();
            try
            {
                AlertConfigDataModel alertConfig = await _context.AlertConfigs.FirstAsync(c => c.Id == id);
                _context.AlertConfigs.Remove(alertConfig);
                await _context.SaveChangesAsync();

                serviceResponse = (_context.AlertConfigs.Include(i => i.AlertNotifications)
                                                             .Include(i => i.Scalings)
                                                             .Select(c => _mapper.Map<AlertConfigResponse>(c))).ToList();


            }
            catch (Exception ex)
            {
                
            }
            return serviceResponse;
        }

        
    }
}
