﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.AlertNotificationUser.Model
{
    public class CreateAlertNotificationUserQuery
    {
        public int AlertNotificationId { get; set; }
        public int UserId { get; set; }
    }
}
