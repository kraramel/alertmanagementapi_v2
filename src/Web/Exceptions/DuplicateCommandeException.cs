﻿using System;

namespace Web.Exceptions
{

    [Serializable]
    public class DuplicateCommandeException : Exception
    {
        public DuplicateCommandeException() : base(Constants.Constants.ValueMustBeUnique) { }
        public DuplicateCommandeException(string message) : base(message) { }
        public DuplicateCommandeException(string message, Exception inner) : base(message, inner) { }
        protected DuplicateCommandeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
