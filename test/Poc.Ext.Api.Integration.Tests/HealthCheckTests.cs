﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using Involys.Poc.Api.Integration.Tests.Utils;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Web;
using Xunit;

namespace Poc.Ext.Api.Integration.Tests
{
    public class HealthCheckTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _fakeApplicationFactory;

        public HealthCheckTests(CustomWebApplicationFactory<Startup> fakeApplicationFactory)
        {
            var projectDir = Directory.GetCurrentDirectory();
            var configPath = Path.Combine(projectDir, "appsettings.Test.json");

            _fakeApplicationFactory = fakeApplicationFactory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureAppConfiguration((context, conf) =>
                {
                    conf.AddJsonFile(configPath);
                });

            });

        }

        [Fact]
        public async Task TestLiveProbeAlwaysReturns200()
        {
            // Arrange
            var httpClient = _fakeApplicationFactory.CreateClient();

            // Act
            var httpResponseMessage = await httpClient.GetAsync("/api/v1/health");

            // Assert
            Assert.NotNull(httpResponseMessage);
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);
        }
    }
}