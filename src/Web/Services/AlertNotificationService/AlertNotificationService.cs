﻿

using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotification.Model;

namespace Web.Services.AlertNotificationService
{
    public class AlertNotificationService : IAlertNotificationService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public AlertNotificationService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<AlertNotificationResponse>> AddAlertNotification(CreateAlertNotificationQuery newAlertNotification)
        {
            List<AlertNotificationResponse> serviceResponse = new List<AlertNotificationResponse>();

            try
            {
                _context.Add(_mapper.Map<AlertNotificationDataModel>(newAlertNotification));
                await _context.SaveChangesAsync();
                List<AlertNotificationDataModel> dbAlertNotifications = await _context.AlertNotifications.ToListAsync();

                serviceResponse = dbAlertNotifications.Select(c => _mapper.Map<AlertNotificationResponse>(c)).ToList();
            }
            catch (Exception ex)
            {
               
            }

            return serviceResponse;
        }
        


        public async Task<List<AlertNotificationResponse2>> GetAllAlertNotifications()
        {
            List<AlertNotificationResponse2> serviceResponse = new List<AlertNotificationResponse2>();

            try
            {
                //Get the Characters from the database
                List<AlertNotificationDataModel> dbAlertNotifications = await _context.AlertNotifications.Include(i => i.AlertConfig)
                                            .Include(x => x.AlertNotificationUsers)
                                            .ThenInclude(x => x.User).ToListAsync();

                serviceResponse = dbAlertNotifications.Select(c => _mapper.Map<AlertNotificationResponse2>(c)).ToList();
            }
            catch (Exception ex)
            {
                
            }
            return serviceResponse;
        }


        public async Task<AlertNotificationResponse2> GetAlertNotificationById(int id)
        {
            AlertNotificationResponse2 serviceResponse = new AlertNotificationResponse2();

            try
            {
                AlertNotificationDataModel dbAlertNotification = await _context.AlertNotifications.Include(i => i.AlertConfig)
                                         .Include(x => x.AlertNotificationUsers).ThenInclude(x => x.User)
                                         .FirstOrDefaultAsync(c => c.Id == id);

                serviceResponse = _mapper.Map<AlertNotificationResponse2>(dbAlertNotification);
            }
            catch (Exception ex)
            {
                
            }
            return serviceResponse;
        }


        public async Task<AlertNotificationResponse2> UpdateAlertNotification(UpdateAlertNotificationQuery updatedAlertNotification)
        {
            AlertNotificationResponse2 serviceResponse = new AlertNotificationResponse2();
            try
            {
                AlertNotificationDataModel alertNotification = await _context.AlertNotifications.Include(i => i.AlertConfig).Include(x => x.AlertNotificationUsers).ThenInclude(x => x.User).FirstOrDefaultAsync(c => c.Id == updatedAlertNotification.Id); alertNotification.Titre = updatedAlertNotification.Titre;
                alertNotification.Description = updatedAlertNotification.Description;
                alertNotification.DateConsultation = updatedAlertNotification.DateConsultation;
                alertNotification.DateEnvoi = updatedAlertNotification.DateEnvoi;
               

                _context.AlertNotifications.Update(alertNotification);
                await _context.SaveChangesAsync();

                serviceResponse = _mapper.Map<AlertNotificationResponse2> (alertNotification);
            }
            catch (Exception ex)
            {
              
            }
            return serviceResponse;
        }

        public async Task<List<AlertNotificationResponse2>> DeleteAlertNotification(int id)
        {
            List<AlertNotificationResponse2> serviceResponse = new List<AlertNotificationResponse2>();
            try
            {
                AlertNotificationDataModel alertNotification = await _context.AlertNotifications
                                                                 .Include(i => i.AlertConfig)
                                                                 .Include(x => x.AlertNotificationUsers).ThenInclude(x => x.User)
                                                                 .FirstAsync(c => c.Id == id);

                _context.AlertNotifications.Remove(alertNotification);
                await _context.SaveChangesAsync();

                serviceResponse = (_context.AlertNotifications.Include(i => i.AlertConfig)
                                                                   .Include(x => x.AlertNotificationUsers).ThenInclude(x => x.User)
                                                                   .Select(c => _mapper.Map<AlertNotificationResponse2>(c))).ToList();


            }
            catch (Exception ex)
            {
               
            }
            return serviceResponse;
        }
    }
}
