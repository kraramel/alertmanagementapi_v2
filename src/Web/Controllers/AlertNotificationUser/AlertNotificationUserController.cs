﻿

using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotificationUser.Model;
using Web.Services.AlertNotificationUserService;

namespace Web.Controllers.AlertNotificationUser

{

    [ApiController]
    [Route("api/[controller]")]
    public class AlertNotificationUserController : ControllerBase
    {
        private readonly IAlertNotificationUserService _alertNotificationUserService;
        public AlertNotificationUserController(IAlertNotificationUserService alertNotificationUserService)
        {
            _alertNotificationUserService = alertNotificationUserService;
        }

        [HttpPost]
        public async Task<IActionResult> AddAlertNotificationUser(CreateAlertNotificationUserQuery newAlertNotificationUser)
        {
            return Ok(await _alertNotificationUserService.AddAlertNotificationUser(newAlertNotificationUser));
        }


    }
}
