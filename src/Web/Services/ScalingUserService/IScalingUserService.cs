﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Controllers.ScalingUser.Model;

namespace Web.Services.ScalingUserService
{
    public interface IScalingUserService
    {
         Task<ScalingResponse2> AddScalingUser(CreateScalingUserQuery newScalingUser);
    }
}
