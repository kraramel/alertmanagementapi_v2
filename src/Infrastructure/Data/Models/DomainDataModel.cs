﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace Infrastructure.Data.Models
{
    public class DomainDataModel
    {
        
        public int Id { get; set; }
        public string DomainName { get; set; }
        public DateTime CreationDate { get; set; }
        public IList<GroupDataModel> groups { get; set; }
        public IList<UserDataModel> users { get; set; }
        public IList<RoleDataModel> roles { get; set; }
        [JsonIgnore]
        public IList<DomainDataModel> SubDomains { get; set; }

        public DomainDataModel Parent { get; set; }
        public bool isHasParent { get; set; }
    }
}
