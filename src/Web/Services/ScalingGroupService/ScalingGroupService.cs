﻿
using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Controllers.ScalingGroup.Model;

namespace Web.Services.ScalingGroupService
{
    public class ScalingGroupService : IScalingGroupService
    {
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        public ScalingGroupService(DatabaseContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<ScalingResponse2> AddScalingGroup(CreateScalingGroupQuery newScalingGroup)
        {
            ScalingResponse2 response = new ScalingResponse2();
            try
            {
                ScalingDataModel scaling = await _context.Scalings
                            .Include(i => i.AlertConfig)
                            .Include(c => c.ScalingGroups).ThenInclude(cs => cs.Group)
                            .FirstOrDefaultAsync(c => c.Id == newScalingGroup.ScalingId);


                if (scaling == null)
                {
                    return response;

                }

                GroupDataModel group = await _context.Groups
                        .FindAsync(newScalingGroup.GroupId);
                if (group == null)
                {
                    return response;
                }

                ScalingGroupDataModel scalingGroup = new ScalingGroupDataModel
                {
                    Scaling = scaling,
                    Group = group
                };



                await _context.ScalingGroups.AddAsync(scalingGroup);

                await _context.SaveChangesAsync();

                response = _mapper.Map<ScalingResponse2>(scaling);
            }
            catch (Exception ex)
            {


            }
            return response;
        }
    }
}
