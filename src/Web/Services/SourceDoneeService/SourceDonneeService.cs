﻿using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.SourceDonnee.Model;
using Web.Exceptions;

namespace Web.Services.SourceDoneeService
{
    public class SourceDonneeService : ISourceDonneeService
    {
        private readonly IMapper _mapper;
        private readonly DatabaseContext _context;

        public SourceDonneeService(IMapper mapper, DatabaseContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<SourceDonneeResponse>> AddSourceDonnee(CreateSourceDonneeQuery newSourceDonnee)
        {
            List<SourceDonneeResponse> serviceResponse = new List<SourceDonneeResponse>();

            _context.Add(_mapper.Map<SourceDonneeDataModel>(newSourceDonnee));
            await _context.SaveChangesAsync();
            List<SourceDonneeDataModel> dbSourceDonnees = await _context.SourceDonnees.ToListAsync();

            serviceResponse = dbSourceDonnees.Select(c => _mapper.Map<SourceDonneeResponse>(c)).ToList();
            return serviceResponse;
        }

        public async Task<List<SourceDonneeResponse>> GetAllSourceDonnees()
        {
            List<SourceDonneeResponse> serviceResponse = new List<SourceDonneeResponse>();

            //Get the Characters from the database
            List<SourceDonneeDataModel> dbScalings = await _context.SourceDonnees.ToListAsync();

            serviceResponse = dbScalings.Select(c => _mapper.Map<SourceDonneeResponse>(c)).ToList();
            return serviceResponse;
        }


        public async Task<SourceDonneeResponse> GetSourceDonneeById(int id)
        {
            SourceDonneeResponse serviceResponse = new SourceDonneeResponse();

            SourceDonneeDataModel dbScaling = await _context.SourceDonnees.FindAsync(id);
            serviceResponse = _mapper.Map<SourceDonneeResponse>(dbScaling);

            return serviceResponse;
        }

        public async Task<SourceDonneeResponse> UpdateSourceDonnee(UpdateSourceDonneeQuery updatedSourceDonnee, int id)
        {
            SourceDonneeResponse serviceResponse = new SourceDonneeResponse();
            try
            {
                SourceDonneeDataModel sourceDonnee = await _context.SourceDonnees.FirstOrDefaultAsync(c => c.Id == id);

                if (sourceDonnee == null)
                {
                    throw new CommandeNotFoundException();
                }

                sourceDonnee.Nom = updatedSourceDonnee.Nom;
                sourceDonnee.Requete = updatedSourceDonnee.Requete;
                sourceDonnee.Table = updatedSourceDonnee.Table;
                

                _context.SourceDonnees.Update(sourceDonnee);
                await _context.SaveChangesAsync();

                serviceResponse = _mapper.Map<SourceDonneeResponse>(sourceDonnee);
            }
            catch (Exception ex)
            {

            }
            return serviceResponse;
        }

        public async Task<List<SourceDonneeResponse>> DeleteSourceDonnee(int id)
        {
            List<SourceDonneeResponse> serviceResponse = new List<SourceDonneeResponse>();
            try
            {
                SourceDonneeDataModel sourceDonnee = await _context.SourceDonnees.FirstAsync(c => c.Id == id);
                _context.SourceDonnees.Remove(sourceDonnee);
                await _context.SaveChangesAsync();

                serviceResponse = (_context.SourceDonnees.Select(c => _mapper.Map<SourceDonneeResponse>(c))).ToList();


            }
            catch (Exception ex)
            {

            }
            return serviceResponse;
        }
    }
}
