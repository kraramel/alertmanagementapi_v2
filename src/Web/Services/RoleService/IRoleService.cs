﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Role.Models;

namespace Web.Services.RoleService
{
    public interface IRoleService
    {
        Task<List<RoleResponse>> GetAllRoles();
        Task<RoleResponse> GetRoleById(int id);
    }
}

