﻿

using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotification.Model;
using Web.Controllers.AlertNotificationUser.Model;
using Web.Exceptions;

namespace Web.Services.AlertNotificationUserService
{
    public class AlertNotificationUserService : IAlertNotificationUserService
    {
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        public AlertNotificationUserService(DatabaseContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<AlertNotificationResponse2> AddAlertNotificationUser(CreateAlertNotificationUserQuery newAlertNotificationUser)
        {
            AlertNotificationResponse2 response = new AlertNotificationResponse2();
            try
            {
                AlertNotificationDataModel alertNotification = await _context.AlertNotifications
                            .Include(i => i.AlertConfig)
                            .Include(c => c.AlertNotificationUsers).ThenInclude(cs => cs.User)
                            .FirstOrDefaultAsync(c => c.Id == newAlertNotificationUser.AlertNotificationId);
                

                if (alertNotification == null)
                {
                    throw new CommandeNotFoundException();
                }

                UserDataModel user = await _context.Users
                        .FindAsync(newAlertNotificationUser.UserId);
                if (user == null)
                {
                    throw new CommandeNotFoundException();
                }
               
                AlertNotificationUserDataModel alertNotificationUser = new AlertNotificationUserDataModel
                { 
                    AlertNotification = alertNotification,
                    User = user
                };



                await _context.AlertNotificationUsers.AddAsync(alertNotificationUser);

                await _context.SaveChangesAsync();

                response = _mapper.Map<AlertNotificationResponse2>(alertNotification);
            }
            catch (Exception ex)
            {
                
                throw;
            }
            return response;
        }
    }
}
