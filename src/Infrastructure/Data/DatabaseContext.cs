﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AlertNotificationUserDataModel>()
                .HasKey(cs => new { cs.AlertNotificationId, cs.UserId });

            modelBuilder.Entity<ScalingUserDataModel>()
                .HasKey(cs => new { cs.ScalingId, cs.UserId });

            modelBuilder.Entity<ScalingGroupDataModel>()
               .HasKey(cs => new { cs.ScalingId, cs.GroupId });

            modelBuilder.Entity<ScalingRoleDataModel>()
               .HasKey(cs => new { cs.ScalingId, cs.RoleId });
        }



        public DbSet<AlertConfigDataModel> AlertConfigs { get; set; }
        public DbSet<AlertNotificationDataModel> AlertNotifications { get; set; }
        public DbSet<AlertNotificationUserDataModel> AlertNotificationUsers { get; set; }
        public DbSet<UserDataModel> Users { get; set; }
        public DbSet<ScalingDataModel> Scalings { get; set; }
        public DbSet<ScalingUserDataModel> ScalingUsers { get; set; }
        public DbSet<GroupDataModel> Groups { get; set; }
        public DbSet<ScalingGroupDataModel> ScalingGroups { get; set; }
        public DbSet<RoleDataModel> Roles { get; set; }
        public DbSet<ScalingRoleDataModel> ScalingRoles { get; set; }
        public DbSet<SourceDonneeDataModel> SourceDonnees { get; set; }
        public DbSet<Commande> Commandes { get; set; }

    }
}
