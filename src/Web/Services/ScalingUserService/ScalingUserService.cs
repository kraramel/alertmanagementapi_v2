﻿

using AutoMapper;
using Infrastructure.Data;
using Infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Controllers.ScalingUser.Model;

namespace Web.Services.ScalingUserService
{
    public class ScalingUserService : IScalingUserService
    {
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        public ScalingUserService(DatabaseContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<ScalingResponse2> AddScalingUser(CreateScalingUserQuery newScalingUser)
        {
            ScalingResponse2 response = new ScalingResponse2();
            try
            {
                ScalingDataModel scaling = await _context.Scalings
                            .Include(i => i.AlertConfig)
                            .Include(c => c.ScalingUsers).ThenInclude(cs => cs.User)
                            .FirstOrDefaultAsync(c => c.Id == newScalingUser.ScalingId);


                if (scaling == null)
                {
                    return response;

                }

                UserDataModel user = await _context.Users
                        .FindAsync(newScalingUser.UserId);
                if (user == null)
                {
                    return response;
                }

                ScalingUserDataModel scalingUser = new ScalingUserDataModel
                {
                    Scaling = scaling,
                    User = user
                };



                await _context.ScalingUsers.AddAsync(scalingUser);

                await _context.SaveChangesAsync();

                response = _mapper.Map<ScalingResponse2>(scaling);
            }
            catch (Exception ex)
            {

                
            }
            return response;
        }
    }
}
