﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Models
{
    public class Commande
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantite { get; set; }
    }
}
