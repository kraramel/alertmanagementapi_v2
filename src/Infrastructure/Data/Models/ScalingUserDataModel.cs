﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class ScalingUserDataModel
    {
        public int ScalingId { get; set; }
        public ScalingDataModel Scaling { get; set; }
        public int UserId { get; set; }
        public UserDataModel User { get; set; }
    }
}
