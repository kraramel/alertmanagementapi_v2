﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class updatingRoleGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AlertConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Designation = table.Column<string>(nullable: true),
                    NiveauCriticite = table.Column<string>(nullable: true),
                    Etat = table.Column<string>(nullable: true),
                    TextAlert = table.Column<string>(nullable: true),
                    Module = table.Column<string>(nullable: true),
                    Frequence = table.Column<string>(nullable: true),
                    SourceDonnee = table.Column<string>(nullable: true),
                    DateDebut = table.Column<DateTime>(nullable: false),
                    DateFin = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlertConfigs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DomainDataModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DomainName = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    isHasParent = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DomainDataModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DomainDataModel_DomainDataModel_ParentId",
                        column: x => x.ParentId,
                        principalTable: "DomainDataModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SourceDonnees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(nullable: true),
                    Table = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SourceDonnees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AlertNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titre = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DateConsultation = table.Column<DateTime>(nullable: false),
                    DateEnvoi = table.Column<DateTime>(nullable: false),
                    AlertConfigId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlertNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AlertNotifications_AlertConfigs_AlertConfigId",
                        column: x => x.AlertConfigId,
                        principalTable: "AlertConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Scalings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nom = table.Column<string>(nullable: true),
                    Conditions = table.Column<string>(nullable: true),
                    AlertConfigId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Scalings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Scalings_AlertConfigs_AlertConfigId",
                        column: x => x.AlertConfigId,
                        principalTable: "AlertConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupName = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    DomainId = table.Column<int>(nullable: false),
                    IsAdminDomain = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_DomainDataModel_DomainId",
                        column: x => x.DomainId,
                        principalTable: "DomainDataModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    DomainId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Roles_DomainDataModel_DomainId",
                        column: x => x.DomainId,
                        principalTable: "DomainDataModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegistrationDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    IsArchived = table.Column<bool>(nullable: false),
                    Profil = table.Column<string>(nullable: true),
                    DomainId = table.Column<int>(nullable: false),
                    IsAdminDomain = table.Column<bool>(nullable: false),
                    ImgPath = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ExpirationPassword = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_DomainDataModel_DomainId",
                        column: x => x.DomainId,
                        principalTable: "DomainDataModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScalingGroups",
                columns: table => new
                {
                    ScalingId = table.Column<int>(nullable: false),
                    GroupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScalingGroups", x => new { x.ScalingId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_ScalingGroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScalingGroups_Scalings_ScalingId",
                        column: x => x.ScalingId,
                        principalTable: "Scalings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScalingRoles",
                columns: table => new
                {
                    ScalingId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScalingRoles", x => new { x.ScalingId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_ScalingRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScalingRoles_Scalings_ScalingId",
                        column: x => x.ScalingId,
                        principalTable: "Scalings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AlertNotificationUsers",
                columns: table => new
                {
                    AlertNotificationId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlertNotificationUsers", x => new { x.AlertNotificationId, x.UserId });
                    table.ForeignKey(
                        name: "FK_AlertNotificationUsers_AlertNotifications_AlertNotificationId",
                        column: x => x.AlertNotificationId,
                        principalTable: "AlertNotifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlertNotificationUsers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScalingUsers",
                columns: table => new
                {
                    ScalingId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScalingUsers", x => new { x.ScalingId, x.UserId });
                    table.ForeignKey(
                        name: "FK_ScalingUsers_Scalings_ScalingId",
                        column: x => x.ScalingId,
                        principalTable: "Scalings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScalingUsers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlertNotifications_AlertConfigId",
                table: "AlertNotifications",
                column: "AlertConfigId");

            migrationBuilder.CreateIndex(
                name: "IX_AlertNotificationUsers_UserId",
                table: "AlertNotificationUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DomainDataModel_ParentId",
                table: "DomainDataModel",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_DomainId",
                table: "Groups",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_DomainId",
                table: "Roles",
                column: "DomainId");

            migrationBuilder.CreateIndex(
                name: "IX_ScalingGroups_GroupId",
                table: "ScalingGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ScalingRoles_RoleId",
                table: "ScalingRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Scalings_AlertConfigId",
                table: "Scalings",
                column: "AlertConfigId");

            migrationBuilder.CreateIndex(
                name: "IX_ScalingUsers_UserId",
                table: "ScalingUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DomainId",
                table: "Users",
                column: "DomainId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlertNotificationUsers");

            migrationBuilder.DropTable(
                name: "ScalingGroups");

            migrationBuilder.DropTable(
                name: "ScalingRoles");

            migrationBuilder.DropTable(
                name: "ScalingUsers");

            migrationBuilder.DropTable(
                name: "SourceDonnees");

            migrationBuilder.DropTable(
                name: "AlertNotifications");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Scalings");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "AlertConfigs");

            migrationBuilder.DropTable(
                name: "DomainDataModel");
        }
    }
}
