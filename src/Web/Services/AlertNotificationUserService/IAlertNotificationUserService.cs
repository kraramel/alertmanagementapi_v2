﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.AlertNotification.Model;
using Web.Controllers.AlertNotificationUser.Model;

namespace Web.Services.AlertNotificationUserService
{
    public interface IAlertNotificationUserService
    {
        Task<AlertNotificationResponse2> AddAlertNotificationUser(CreateAlertNotificationUserQuery newAlertNotificationUser);
    }
}
