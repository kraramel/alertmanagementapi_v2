﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Models
{
    public class ScalingGroupDataModel
    {
        public int ScalingId { get; set; }
        public ScalingDataModel Scaling { get; set; }
        public int GroupId { get; set; }
        public GroupDataModel Group { get; set; }
    }
}
