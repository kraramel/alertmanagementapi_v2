﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class NotificationType
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Sujet { get; set; }
        public string Message { get; set; }
        public int TempsRappel { get; set; }
    }
}
