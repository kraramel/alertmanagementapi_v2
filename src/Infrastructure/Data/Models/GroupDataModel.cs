﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Infrastructure.Data.Models
{
    public class GroupDataModel
    {
       
        public int Id { get; set; }
        public string GroupName { get; set; }
        public DateTime CreationDate { get; set; }
        public int DomainId { get; set; }
        [JsonIgnore]
        public DomainDataModel Domain { get; set; }
        // public IList<UserDataModel> Users { get; set; }
        
/*        public IList<UserGroup> UserGroup { get; set; }
*/        public bool IsAdminDomain { get; set; }
/*        public IList<RoleGroup> RoleGroup { get; set; }
*/    }
}
