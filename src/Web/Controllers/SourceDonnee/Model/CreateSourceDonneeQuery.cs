﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Controllers.SourceDonnee.Model
{
    public class CreateSourceDonneeQuery
    {
        public string Nom { get; set; }
        public string Table { get; set; }
        public string Requete { get; set; }

    }
}
