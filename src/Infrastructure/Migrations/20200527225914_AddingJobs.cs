﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class AddingJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfExecution",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfExpiration",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "DayOfMonth",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EveryHoursAtMinutes",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ExecuteEveryHours",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ExecuteEveryMinutes",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "HourOfExecuute",
                table: "AlertConfigs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulFriday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulMonday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulSaturday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulSunday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulThursday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulTuesday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "SchedulWednesday",
                table: "AlertConfigs",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOfExecution",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "DateOfExpiration",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "DayOfMonth",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "EveryHoursAtMinutes",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "ExecuteEveryHours",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "ExecuteEveryMinutes",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "HourOfExecuute",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulFriday",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulMonday",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulSaturday",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulSunday",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulThursday",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulTuesday",
                table: "AlertConfigs");

            migrationBuilder.DropColumn(
                name: "SchedulWednesday",
                table: "AlertConfigs");
        }
    }
}
