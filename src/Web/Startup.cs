using AutoMapper;
using Common.Email;
using Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Web.Services.AlertConfigService;
using Web.Services.AlertNotificationService;
using Web.Services.AlertNotificationUserService;
using Web.Services.GroupService;
using Web.Services.RoleService;
using Web.Services.ScalingService;
using Web.Services.ScalingUserService;
using Web.Services.SourceDoneeService;
using Web.Services.UserService;
using Hangfire;
using Hangfire.SqlServer;
using System;
using Web.JobSchedulling.Hangfire;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            
            // Add Hangfire services.
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));

            // Add the processing server as IHostedService
            services.AddHangfireServer();

            var emailConfig = Configuration
                            .GetSection("EmailConfiguration")
                            .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddScoped<IEmailSender, EmailSender>();

            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddScoped<IAlertConfigService, AlertConfigService>();

            services.AddScoped<IAlertNotificationService, AlertNotificationService>();
            services.AddScoped<IAlertNotificationUserService, AlertNotificationUserService>();

            services.AddScoped<IScalingService, ScalingService>();
            services.AddScoped<IScalingUserService, ScalingUserService>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ISourceDonneeService, SourceDonneeService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IRecurringJobManager recurringJobManager, IServiceProvider serviceProvider, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            /*  app.UseHttpsRedirection();*/

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseHangfireDashboard();
            backgroundJobs.Enqueue(() => Console.WriteLine("Hello world from Hangfire!"));

            //Adding our Schedulled jobs to Hangfire
            var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            DatabaseContext context = new DatabaseContext(optionsBuilder.Options);
            var JobToLaunch = new HangfireJobScheduler(context);
            RecurringJob.AddOrUpdate(() => JobToLaunch.SchedulerRucurringJobs(), Cron.Minutely);

            /* recurringJobManager.AddOrUpdate(
                 "Run every minute",
                 () => serviceProvider.GetService<IScalingService>().AddNotif(2),
                 "* * * * *"
                 );*/
        }
    }
}
