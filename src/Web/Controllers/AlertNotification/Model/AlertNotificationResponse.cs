﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.User.Model;

namespace Web.Controllers.AlertNotification.Model
{
    public class AlertNotificationResponse
    {
        public int Id { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public DateTime DateConsultation { get; set; }
        public DateTime DateEnvoi { get; set; }
        public List<UserResponse> Users { get; set; }       
        



    }

}
