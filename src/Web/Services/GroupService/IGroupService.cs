﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Group;

namespace Web.Services.GroupService
{
    public interface IGroupService
    {
        Task<List<GroupResponse>> GetAllGroups();
        Task<GroupResponse> GetGroupById(int id);
    }
}
