﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Models
{
    public class SourceDonneeDataModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Table { get; set; }
        public string Requete { get; set; }

    }
}
