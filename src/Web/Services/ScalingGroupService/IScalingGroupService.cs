﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers.Scaling.Model;
using Web.Controllers.ScalingGroup.Model;

namespace Web.Services.ScalingGroupService
{
    public interface IScalingGroupService
    {
        Task<ScalingResponse2> AddScalingGroup(CreateScalingGroupQuery newScalingUser);
    }
}
