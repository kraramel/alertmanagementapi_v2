﻿namespace Web.Constants
{
    public static class Constants
    {

        public const string CommandeNotFound = "Commande not found";
        public const int NoValue = -1;

        public const string ValueMustBeUnique = "The value must be unique";

        //Frequencies
        public const string EveryMinute = "Every Minute";
        public const string EveryHour = "Every Hour";
        public const string EveryDay = "Every Day";
        public const string DaysOfWeek = "Days of week";
        /*    public const string EveryWeek = "Every Week";*/
        public const string EveryMonth = "Every Month";
        public const string AtGivenDateHour = "At Given Date Hour";

        //List of possible jobs
        public const string Executable = "Executable";
        public const string WebService = "Web Service";
        public const string Script = "Script";
        public const string Command = "Command";
        public const string StoredProc = "Stored Procedure";

        //Case of stored procedures
        public const string StoredProcSqlServer = "Microsoft SQL Server";
        public const string StoredProcMYSQLServer = "MySQL Server";
        public const string StoredProcPostgreSQL = "PostgreSQL";
        public const string StoredProcOracleServer = "Oracle Server";

        //Auth Type in case of Sql Server
        public const string AuthWindows = "Authentification Windows";
        public const string AuthSQlServer = "Authentification SqlServer";

        //Checking if the job was schedulled when frequency is selected to AtGivenDate
        public const string IsSchedulled = "IsSchedulled";
        public const string IsNotSchedulled = "Not Schedulled";

        //Address mail of notification manager
        public const string NotificationMangerEmail = "Notification.Manager@involys.com";

        //Terminals for different operating systems
        public const string WindowsTerminal = "cmd.exe";
        public const string LinuxMacTerminal = "/bin/bash";
    }
}
 